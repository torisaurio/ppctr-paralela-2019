#!/usr/bin/env bash

#Script que automatiza el benchmark, probando con diferente nuemero de iteraciones


#Estos parametros se pueden cambiar a elección propia
ejec=2000000000

max=11
nthreads=4

#n procesos
printf "numero de procesos antes de ejecutar\n"
ps aux --no-heading | wc -l

#secuencial secuencial sin optimizar
printf "ejecucion secuencial normal\n"
	for i in `seq 1 1 $max`;
	do
		./a.out $ejec
	done
#secuencial secuencial optimizada
printf "make seq\n"
	make seq
printf "ejecucion secuencial optimizada\n"
	for i in `seq 1 1 $max`;
	do
		./build/seq $ejec
	done
#ejecucion cpp
printf "make cpp\n"
	make cpp
	printf "ejecucion cpp $i\n"
	for i in `seq 1 1 $max`;
	do
		./build/cpp $ejec $nthreads
	done
#ejecucion omp
printf "make omp\n"
		make omp
	printf "ejecucion omp $i\n"
	for i in `seq 1 1 $max`;
	do
		./build/omp $ejec $nthreads
	done

#n procesos
printf "numero de procesos antes de ejecutar\n"
ps aux --no-heading | wc -l
