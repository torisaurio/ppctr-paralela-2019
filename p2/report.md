# Título de práctica

Autor Mario Abad Diaz
Fecha 16/12/2019

## Prefacio

Objetivos conseguidos

Breve descripción de lo que te ha parecido la práctica. Aprendizajes, dificultades, cosas interesantes, qué te hubiera gustado hacer o mejorarías, qué conceptos te gustaría haber practicado de C++/OMP, opinión sobre el informe, etc

Esta practica ha sido una practica que ha tenido una dificultad media pero ha servido para ver las diferencias de rendimiento entre los hilos de omp y c++.
Las dificultades las he obtenido al empezar a utilizar omp ya que es la primera vez que lo utilizaba.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

Descripcion del sistema donde se realiza el *benchmarking*.

-Ejecucion sobre Ubuntu Desktop 16.04 LTS 64bits

-7,7GiB RAM(obtenido en opciones about this computer)

-4 Cores fisicos(obtenido en la web de intel)

-4 Cores logicos(obtenido en la web de intel)

-CPU Intel Core i7-7700HQ(obtenido en opciones about this computer)

-CPU 8 cores(obtenido en opciones about this computer)

-CPU @1.9GHz(fijada con cpufreq usermode)

-Cache size(obtenido en /proc/cpuinfo): 6144KB

-CPU flags(obtenidas en /proc/cpuinfo): fpu vme de pse tsc msr pae mce cx8 apic sep mtrr
 pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb
 rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid
  aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg
  fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave
  avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs
  ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2
  erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves
  dtherm ida arat pln pts hwp hwp_notify hwp_act_window
 hwp_epp md_clear flush_l1d

-numero de procesos promedio(obtenido con ps aux ?no-heading | wc -l)= 242


## 2. Diseño e Implementación del Software

En este apartado se describirá la estructura del software desarrollado justificando las  decisiones que se hayan tomado en su diseño.

Si se cree necesario, se puede aportar código fuente, pero lo normal será describir los objetos que se propongan para desarrollar la práctica y la funcionalidad de los mismos, ya que el código fuente se entregará por separado.

Si se quiere enseñar código fuente se puede aportar en bloques de código simplificado (solo lo relevante, tal y como yo hago en las diapositivas del Tema 2) o indicar las líneas de código del fichero en cuestión. Ojo con la segunda opción ya que cada vez que toques el código fuente podrán cambiar.

OPTIMIZACION

Para optimizar el codigo original dado lo he simplificado. He empezado eliminando el fichero p2.hpp para lo que he eliminado los structs que contiene y he creado las variables en el fichero p2.cpp. Lo siguiente que elimine fueron los metodos attach y parse, donde me di cuenta de que A, B y C son constantes y vi como se calculaban ciertas variables que movi al main simplificando la forma de la que se calculan eliminando casts innecesarios. Finalmente simplifique el calculo del metodo sequential calculando todos los numeros que eran constantes ademas de eliminando los las potencias innecesarias cambiandolas por multiplicaciones que son mas rapidas de calcular y eliminando los casts innecesarios.

Otra optimizacion es la que esta explicada en el main en el apartado de omp respecto a la forma de calcular las iteraciones del bucle for.

PREPROCESADOR Y LIBRERIAS

Al comienzo del codigo hago los includes de las librerias que seran necesarias para la realizacion de la practica.
A continuacion defino las constantes A, B y C que son necesarias para el calculo de la elipse.

VARIABLES GLOBALES

Como unica variable global hay definido un mutex que se utilizara para sincronizar el proceso de guardar el resultado en los hilos de c++.

MAIN

En el main empezamos recibiendo el numero de iteraciones que tiene tamaño long. Si no se pasa ningun numero de iteraciones como argumento al llamar al programa este no se ejecuta.

El main restante se divide en 3 partes, una es la ejecucion c++, otra es la de omp y la ultima la secuencial. Cada uno de estos segmentos estan hechos con compilacion condicional de forma que solo se compilara lo necesario para ejecutar cada uno de ellos.

El primer segmento que nos encontramos es el de c++ en el que lo primero que se hace es recibir el numero de hilos mediante una variable de entorno que se llama NUM_HILOS y despues se recibe como argumento de forma que si se escriben las 2 predomine el numero pasado como argumento. Si no se pasa ninguno de los 2 el programa termina.
Creamos el array de hilos de c++ y calculamos la variable gap que sera utilizada mas tarde para calcular la elipse. Creamos los hilos (que ejecutaran el metodo ellipse) pasandoles como argumento el gap que acabamos de calcular, el numero de iteraciones total, la variable donde guardaran el resultado y el numero de hilos.
Por ultimo hacemos join de los hilos y calculamos el resultado final y lo imprimimos por pantalla.

La siguiente seccion que encontramos es la que corresponde a la optimizacion de openmp. En ella se recibe el numero de hilos de la misma forma que en la ejecucion de c++. Comenzando con las instrucciones de openmp tenemos omp_set_num_threads que establece el numero de hilos de la siguiente region paralela. la siguiente instruccion que encontramos es pragma omp parallel que lo que hace es crear una region paralela en la que crea el numero de hilos que le hemos establecido primero o el que viene por defecto que suele ser el numero de cores de la cpu que estemos usando. Pragma omp parallel permite que se le inserten clausulas. En el codigo se utiliza private que lo que hace es establecer las variables que se le pasen como privadas entre los hilos. shared hace que las variables que se le pasen sean compartidas por todos los hilos. reduction realiza una operacion que se le pasa (en este caso suma) al final de la ejecucion de todos los hilos, utilizando el ultimo valor que toma la variable dentro de ellos para asignar el total de realizar la operacion seleccionada a las variables de los hilos a esta variable. Dentro de la region parallel encontramos la instruccion omp_get_thread_num que devuelve el numero de hilo que lo llama. por ultimo encontramos un for que funciona de la misma forma que en c++.

Finalmente tenemos la ejecucion secuencial en la que simplemente se ejecuta la operacion de forma secuencial y se imprime el resultado por pantalla.

ELLIPSE

Este es el metodo que ejecutan los hilos cuando se crean. En el, hay un bucle for que calcula un resultado parcial de la misma forma que el bucle en omp para que sea lo mas eficiente posible.
Finalmente se guarda el resultado en la variable pasada como parametro utilizando un mutex para sincronizar los accesos.


## 3. Metodología y desarrollo de las pruebas realizadas

Descripción detallada del proceso de benchmarking, captura de resultados y generación de gráficas. Todo lo necesario para comenzar el análisis.

Aquí se describirán las pruebas realizadas, su intención y los resultados obtenidos. Se incluirá el desarrollo de programas de prueba adicionales si han sido necesarios.

Podéis incluir vuestros scripts de ejecución en el propio informe (o llevarlos a Anexos si lo crees conveniente), así como indicar cómo los habéis utilizado y por qué:

Utilizando los resultados de la practica 1 que dio 8 como numero de hilos optimo para mi ordenador, realizare los benchmarks (c++ y omp) de forma que utilizen 8 hilos ya que el objetivo sobre todo es comparar cpp con omp y la version secuencial dada con la optimizada, comparar hilos con la secuencial es algo mas secundario ya que ya se hizo en la practica 1.

Para realizar los benchamarks se han utilizado 2 scripts de la misma manera que en la practica anterior. tenemos el script automatized_exec_opt.sh que es exactamente igual al de la practica anterior ya que solo llama al programa 2 veces esperando 1 segundo entre llamadas. El otro script es muy parecido solo que esta vez los scripts se realizan con 1 solo numero de hilos.
Al igual que en la practica anterior se hacen 2 estaciones con 11 iteraciones cada una descartando la primera como calentamiento.

Grafica 200.000 iteraciones

![](./imagenes/speedup200000.png)

A la vista de esta grafica podemos concluir que la optimizacion de la parte secuencial es muy significativa ya que es casi 2,5 veces mas rapida. en cuanto a la comparacion con c++ podemos ver que con estas iteraciones no se saca demasiado partido a los hilos ya que es solo 1,21 veces mas rapida. La sorpresa es al comparar cpp con omp donde vemos que omp es 9 veces mas rapido en este numero de iteraciones. Aqui podemos ver que openmp tarda un tiempo considerable en levantar los hilos respecto a la ejecucion en cpp. Debido al bajo numero de iteraciones, los resultados de esta prueba han sido algo inestables para lo que se han realizado 2 tandas mas de benchmarks para consolidar los resultados.

Grafica 2.000.000.000 iteraciones

![](./imagenes/speedup2000000000.png)

En esta segunda grafica podemos observar resultados mas solidos ya que el numero de iteraciones es alto y el tiempo de ejecucion es de unos pocos segundos en modo secuencial. Podemos observar como es speedup entre el codigo secuencial optimizado y si optimizar es el mismo, sin embargo en el speedup de el codigo secuencial optimizado y el paralelo de cpp esta vez podemos observar un speedup de casi 4, lo cual tiene mas logica que en la ejecucion anterior aunque no acaba de ser cercano al casi 6 de la practica anterior. La explicacion que encuentro a este dato es que puede que solo los cores fisicos esten aprovechando la paralelizacion ya que los cores logicos carecen de un hardware propio completo. Tras darme cuenta de este dato decidi probar el programa con 4 hilos y encontre que mi suposicion era cierta como muestro en una columna que añadi despues. Por ultimo en la tercera barra podemos observar la diferencia entre cpp y omp de una forma mas precisa y comprobamos que la ejecucion con hilos de cpp tiene un speedup de 1,25 respecto a la ejecucion de omp que introduce un overhead en la gestion de los hilos.


## 4. Discusión

En este apartado se aportará una valoración personal de la práctica, en la que se destacarán aspectos como las dificultades encontradas en su realización y cómo se han resuelto.

También se responderá a las cuestiones formuladas si las hubiere.

En esta practica las dificultades que he encontrado han sido al utilizar variables de entorno y al utilizar variables de preprocesador desde la fase de compilado. He resuelto estos problemas utilizando internet y haciendo pequeños programas de prueba.
