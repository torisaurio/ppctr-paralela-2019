#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <mutex>
#include <thread>
#include <omp.h>
#include <time.h>
#include <sys/time.h>

#define A  1.5f
#define B  2.4f
#define C  1.0f

#if EJEC_CPP == 1
std::mutex tmp_m; //tmp mutex
#endif
void ellipse(double gap, long numIts, double* finalTmp, int paso, int nHilos);

int main(int argc, char* argv[])
{
  long numIts = -1;
  if(argc>= 2){
    numIts = atol(argv[1]);
  }
  if(numIts<=0){
    exit(-1);
  }
  struct timeval start, end;
  //c++
  gettimeofday(&start, NULL);
  #if EJEC_CPP == 1
    int nHilos=0;
    if(getenv("NUM_HILOS")!=0){
      nHilos = atoi(getenv("NUM_HILOS"));
    }
    //ejec paralela
    if(argc >= 3){
      nHilos = atoi(argv[2]);
    }
    if(nHilos<=0){
      printf("numero de hilos incorrecto\n");
      exit(-1);
    }
    std::thread threads[nHilos];//numero de hilos de mi procesador
    int i;
    double finalTmp=0;
    double gap = 1.0/(double)numIts;
    //inicializa hilos
    for(i = 0; i<nHilos; i++){
      threads[i] = std::thread(ellipse,gap,numIts,&finalTmp, i,nHilos);
    }
    //termina hilos
    for(i = 0; i<nHilos; i++){
      threads[i].join();
    }
    //calcula resultado y lo imprime por pantalla
    printf("CPP la elipse vale: %f \n", finalTmp* gap * 4.082482901);
  #endif
  //omp
  #if EJEC_OMP == 1
    int nHilos=0;
    if(getenv("NUM_HILOS")!=0){
      nHilos = atoi(getenv("NUM_HILOS"));
    }
    //ejec paralela
    if(argc >= 3){
      nHilos = atoi(argv[2]);
    }
    if(nHilos<=0){
      printf("numero de hilos incorrecto\n");
      exit(-1);
    }
    int i;
    double gap = 1.0/(double)numIts;
    double tmp;
    int nHilo;

    omp_set_num_threads(nHilos);
    #pragma omp parallel private(i,nHilo) shared(numIts,nHilos,gap) reduction(+:tmp)
    {
      nHilo = omp_get_thread_num();
      for(i = nHilo; i<numIts; i= i+nHilos){
        tmp += 4/(1+(((i+0.5)*gap)*((i+0.5)*gap)));
      }
    }
    printf("OMP ellipse: %f \n", tmp* gap * 4.082482901);
  #endif
  //secuencial
  #if EJEC_NORMAL == 1
    double tmp;
    double gap = 1/(double)numIts;
    for (int i=0; i<=numIts; i++) {
      tmp = tmp + 4/(1+(((i+0.5)*gap)*((i+0.5)*gap)));
    }
    printf("seq ellipse: %f \n", tmp* gap * 4.082482901);
  #endif
  gettimeofday(&end, NULL);
  double t= (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
  printf("Interval time: %f ms\n", t);
  return 0;
}

#if EJEC_CPP
void ellipse(double gap, long numIts, double* finalTmp, int paso, int nHilos){
  int i;
  double tmp = 0;
  for (i=paso; i<numIts; i = i + nHilos) {
    tmp = tmp + 4/(((i+0.5)*gap)*((i+0.5)*gap)+1);
  }
  std::lock_guard<std::mutex> guard(tmp_m);
  *finalTmp = *finalTmp + tmp;
}
#endif
