# Titulo de practica
Autor Mario Abad Diaz
Fecha 11/12/1999
## Prefacio
Objetivos conseguidos
Breve descripcion de lo que te ha parecido la practica. Aprendizajes, dificultades,
cosas interesantes, que te hubiera gustado hacer o mejorarias, que conceptos te gustaria
haber practicado de C++/OMP, opinion sobre el informe, etc

La practica 1 para mi ha sido la practica mas complicada con diferencia y un gran impacto
a la hora de como afrontar la asignatura. Esta practica me ha ayudado a comprender mucho
mejor la forma de trabajar en proyectos ya que hasta ahora todas las practicas que habiamos
hecho habian sido mucho mas guiadas y sin tener que buscarnos tanto la vida. Las mayores
dificultades se presentaron al finalizar la practica. Entre ellas estan la realizacion
del FEATURE_LOGGER y de los benchmarks.

## indice
1. Sistema
2. Diseño e Implementacion del Software
3. Metodologia y desarrollo de las pruebas realizadas
4. Discusion
5. Propuestas optativas

## 1. Sistema

Descripcion del sistema donde se realiza el *benchmarking*.

-Ejecucion sobre Ubuntu Desktop 16.04 LTS 64bits

-7,7GiB RAM(obtenido en opciones about this computer)

-4 Cores fisicos(obtenido en la web de intel)

-4 Cores logicos(obtenido en la web de intel)

-CPU Intel Core i7-7700HQ(obtenido en opciones about this computer)

-CPU 8 cores(obtenido en opciones about this computer)

-CPU @1.9GHz(fijada con cpufreq usermode)

-Cache size(obtenido en /proc/cpuinfo): 6144KB

-CPU flags(obtenidas en /proc/cpuinfo): fpu vme de pse tsc msr pae mce cx8 apic sep mtrr
 pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb
 rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid
  aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg
  fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave
  avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs
  ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2
  erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves
  dtherm ida arat pln pts hwp hwp_notify hwp_act_window
 hwp_epp md_clear flush_l1d

-numero de procesos promedio(obtenido con ps aux ?no-heading | wc -l)= 243

## 2. Diseño e Implementacion del Software
En este apartado se describira la estructura del software desarrollado justificando las
decisiones que se hayan tomado en su diseño.
Si se cree necesario, se puede aportar codigo fuente, pero lo normal sera describir los
objetos que se propongan para desarrollar la practica y la funcionalidad de los mismos,
ya que el codigo fuente se entregara por separado.
Si se quiere enseñar codigo fuente se puede aportar en bloques de codigo simplificado
(solo lo relevante, tal y como yo hago en las diapositivas del Tema 2) o indicar las
lineas de codigo del fichero en cuestion. Ojo con la segunda opcion ya que cada vez que
toques el codigo fuente podran cambiar.
Puedes apoyarte en esquemas o graficos para definir tu diseño o interacciones entre hilos
(recomendable formato png):


PREPROCESADOR
Al principio del codigo como es logico se encuentran los #include de las librerias de
las funciones que se utilizaran a lo largo del codigo. A continuacion, se encuentran
los #define entre los que se encuentran FEATURE_LOGGER, DEBUG y FEATURE_OPTIMIZE.
Los 3 toman valores entre 0 y 1 dependiendo de si estan o no activados.

STRUCT
Utilizo un struct llamado Parametros para pasar los parametros a los hilos y de esta
forma dar mas claridad al codigo. Este struct contiene nhilo que se utiliza
principalmente en el FEATURE_LOGGER para saber que hilo proporciona cada resultado.
inicio, limite, inicioExtra, limiteExtra son punteros de array (que contiene el array
de elementos completo) que indican las secciones que debe de calcular dicho hilo.
resultadoLogger es el resultado total del hilo que se le pasa al hilo FEATURE_LOGGER
y resultado es el resultado total de todos los hilos.

VARIABLES GLOBALES
He creado variables globales para los mutex, las variables condicionales, booleanos
de las variables condicionales, el atomico del FEATURE_OPTIMIZE y por ultimo una
variable que contiene el numero de hilo llamada nHilo con la que se comunican el hilo
FEATURE_LOGGER y los hilos que operan para que el hilo FEATURE_LOGGER pueda imprimir
por pantalla los resultados parciales por orden de creacion del hilo. Las variables
globales las he utilizado para simplificar el codigo.

MAIN
Comienzo la funcion main.
Para comenzar obtenemos los parametros que se han pasado por la CLI al llamar al programa.
Lo primero obtenemos el numero de elementos utilizando atoi para convertir la cadena de
caracteres a numero.  Con este numero hacemos reserva en memoria dinamica para crear un
array de double que posteriormente inicializamos cada posicion del array a su indice.
Se utiliza memoria dinamica ya que este array puede tener un gran numero de elementos.
Se guarda la operacion en una variable que posteriormente sera utilizada para elegir
la forma de operar.

El main se divide en dos partes, la parte secuencial y la parte paralela. Se entra
a la parte paralela utilizando 4 argumentos en la CLI siendo el 3 ??multi-thread?
y el 4 el numero de hilos a utilizar en la parte paralela.
En la parte paralela al comienzo se recoge el numero de hilos de la CLI y se
guarda en la variable nHilos, posteriormente se crea la variable nHilosOp,
que contiene el numero de hilos que van a realizar operaciones. En el caso de
que el FEATURE_LOGGER este desactivado seran todos los hilos, pero si esta activado
sera nHilos-1 ya que he considerado que el numero de hilos que se pasa como parametro
en la CLI es absoluto y no se pueden utilizar mas hilos que los indicados. Por ello
al utilizar el FEATURE_LOGGER se debe de asignar un hilo a esta caracteristica.
Se hacen comprobaciones para que (como se indica en el enunciado de la practica)
el numero de hilos se encuentre entre 1 y 12 y tambien para que en el caso de que
el numero de operaciones sea menor que el numero de hilos, se ajuste el numero de
hilos igual al numero de elementos ya que seria absurdo tener mas hilos que elementos
a operar porque habria hilos que no harian ningun trabajo.
Se hacen los calculos para asignar las porciones de trabajo adecuadas a cada hilo.
Para ello se calcula resto que es una variable que contiene el resto de dividir el
numero de elementos entre el numero de hilos. Este resto es el trabajo que tendra
que hacer extra el primer hilo en acabar su trabajo. tamHilo contiene el numero de
elementos con el que operara cada hilo. Si hay resto se inicializan inicioExtra y
limiteExtra que son variables que contienen la posicion inicial y la final del
fragmento extra que tendra que calcular el hilo que primero acabe. En caso de ser
resto=0 se inicializan a -1 que indica que no hay parte extra.
Si esta activado FEATURE_LOGGER se crean 3 variables. La primera es una condition
variable llamada logger_acaba que se utilizara mas tarde para avisar al main de
que el hilo FEATURE_LOGGER ha terminado de operar, la segunda resultadoLogger que
contendra el resultado que le pasara el hilo FEATURE_LOGGER al main y por ultimo
op que contiene un id de la operacion que se esta realizando (suma, resta o xor)
que se utilizara mas tarde para indicar al FEATURE_LOGGER el tipo de operacion
que tiene que realizar con los resultados.
A continuacion, hago un bucle for que servira para inicializar los sctructs
que pasare a los hilos y tambien para inicializar los hilos. Dependiendo de la
operacion que se indicase en la CLI se inicializa a los hilos con el metodo que
corresponda. En el caso de que el hilo FEATURE_LOGGER este activado tambien se
inicializa. Posteriormente se hace el join de los hilos dependiendo de si esta el hilo
FEATURE_LOGGER activado o no. En el caso de que no este activado, se hace
join de los hilos y se retorna 0 ya que no ha habido errores al ejecutar. En el
caso de que este activado el hilo FEATURE_LOGGER primero se hace join a los hilos que
realizan operaciones, despues sincronizo al hilo FEATURE_LOGGER con el main con una
variable condicional como se indica en el enunciado. Obtengo el resultado del hilo
FEATURE_LOGGER y lo comparo con el resultado de los hilos, dependiendo de si
FEATURE_OPTIMIZE esta activado o no lo comparo con el resultado normal en caso de
que no esta activado o con el resultado atomico en caso de que si este activado.
Si el resultado del hilo FEATURE_LOGGER coincide con el resultado de los hilos se
retorna 0 y en caso de que no sea asi se retorna 1. Si por el contrario se entra
en la parte secuencial, se calcula el resultado de la operacion recibida por la CLI
y se retorna 0.

FEATURE_LOGGER
El hilo FEATURE_LOGGER consiste en un hilo que recibe los resultados de todos los
hilos que operan por medio de variables condicionales, suma el total y se lo envía
con otra variable condicional al main. Para implementar esto se crea un array de
tamaño nHilosOp donde se guardarán los resultados de los hilos que operan, se crea
un unique_lock del mutex para utilizarlo con la variable condicional. A continuación,
se hace un bucle while cuya condición de salida es cont<0, cont es una variable que
cuenta los hilos que todavia no han pasado el resultado al hilo FEATURE_LOGGER.
Dentro el bucle se hace un notify a logger_ready que es una variable condicional que
indica a los hilos que operan que el hilo FEATURE_LOGGER está listo para recibir
notificaciones. El hilo FEATURE_LOGGER hace wait a hilo_acaba que es otra variable
condicional que notificaran los hilos cuando vayan acabando. He utilizado 2 variables
condicionales ya que nada te garantiza que el hilo FEATRURE_LOGGER vaya a estar en el
wait antes de que algún hilo acabe y le haga un notify, caso en el que se perderían
resultados porque en mi codigo los resultados los pasan los hilos de 1 en 1 al hilo
FEATURE_LOGGER y este los guarda en un array. Las 2 variables condicionales hacen que
los hilos esperen antes de mandar los resultados a que el hilo FEATURE_LOGGER les
notifique para que le puedan pasar los resultados. En ese momento el hilo notificado
enviará al hilo FEATURE_LOGGER por medio de una variable pasada como referencia a ambos
el resultado y tambien cambia el valor de una variable global que contiene el numero
de hilo al que pertenece el resultado. Entonces el hilo FEATURE_LOGGER guarda dicho
resultado en la posicion del array indicada por el numero de hilo de la variable global.
Por ultimo el logger calcula el resultado total operando el resultado parcial de cada
hilo, guarda el resultado en una variable pasada como referencia (variable que se
usaba para comunicarse con los hilos) y notifica al hilo main.

SUM, SUB y XOR

estos son los metodos con los que se crean los hilos que operan y funcionan todos
de la misma forma pero cambiando la operación que se realiza.
Lo primero que se hace es calcular el resultado de la porcion de elementos que le
toca al hilo y despues hay otro bucle para calcular el resto. En este bucle unicamente
entrara el primer hilo que acabe su trabajo. Para garantizar esto una de las condiciones
de entrada al bucle es poseer un mutex el cual solamente poseera el hilo que primero lo
tome ya que despues no será liberado para evitar que otro hilo lo pueda coger mas tarde.
Si el hilo FEATURE_LOGGER esta activado se hace un wait hasta que se notifique al hilo
que el hilo FEATURE_LOGGER esta listo para recibir un resultado. Despues de recibir la
notificacion guarda el resultado en la variable pasada por referencia y cambia la variable
global con el numero de hilo al numero de este hilo y por ultimo notifica al hilo
FEATURE_LOGGER para que recoja el resultado.
Si FEATURE_OPTIMIZE esta desactivado se toma un mutex con lock_guard para acceder a la
variable de resultado de forma sincronizada evitando problemas. En caso de que
FEATURE_OPTIMIZE estuviera activado se operaria el contenido de la variable atomica
con el resultado del hilo y se guardaria en la variable atomica aprovchando sus
caracteristica de atomicidad y asi evitando tomar un mutex para garantizar la exclusion mutua.


## 3. Metodologia y desarrollo de las pruebas realizadas
descripcion detallada del proceso de benchmarking, captura de resultados y generacion
de graficas. Todo lo necesario para comenzar el analisis.
aqui se describiran las pruebas realizadas, su intencion y los resultados obtenidos.
Se incluira el desarrollo de programas de prueba adicionales si han sido necesarios.
podeis incluir vuestros scripts de ejecucion en el propio informe (o llevarlos a Anexos
  si lo crees conveniente), asi como indicar como los habeis utilizado y por que:
Algunos codigos de ejemplo:

SCRIPTS

Para realizar el proceso de benchmarking es necesario utilizar un entorno constante y
con los menores cambios posibles para que el proceso sea justo y preciso. Para lograr
esto el primer paso que realice fue intentar cambiar la frecuencia de los cores del
procesador. Al principio lo intente utlizando

sudo cpufreq-set -c n freqGHz (siendo n el numero del core y freq la frecuencia en GHz)

pero esto fue imposible debido a que en el driver actual de Intel no se puede usar el
governor userspace  por lo que tuve que deshabilitar el driver Intel_pstate al arrancar
el sistema. De esta forma el sistema arranca con el driver antiguo de Intel el cual si
tiene el governor userspace. Al utilizar este driver no tuve ningun problema en establecer
los cores a una frecuencia fija. Para ello utilice el siguiente script llamado cpu.sh

#!/usr/bin/env bash
sudo cpufreq-set -f 1.9GHz -c 0
sudo cpufreq-set -f 1.9GHz -c 1
sudo cpufreq-set -f 1.9GHz -c 2
sudo cpufreq-set -f 1.9GHz -c 3
sudo cpufreq-set -f 1.9GHz -c 4
sudo cpufreq-set -f 1.9GHz -c 5
sudo cpufreq-set -f 1.9GHz -c 6
sudo cpufreq-set -f 1.9GHz -c 7

cpufreq-info

este script cambia la frecuencia de todos los cores a 1.9GHz y muestra la informacion detallada
de cada core (con lo que se puede comprobar que la frecuencia se ha cambiado).

Para realizar las ejecuciones de forma automatizada he utilizado 2 scripts.
Uno simplemente llama al otro 2 veces haciendo un sleep de 1 segundo despues de
finalizar la primera llamada. Llama 2 veces para de esta forma tener 2 estaciones y que
los resultados sean mas precisos.

#!/usr/bin/env bash

for iterations in 1 2; do
		printf "estacion $iterations\n"
		./benchsuite.sh
		sleep 1
	done

el fichero benchsuite.sh al que se llama arriba contiene las llamadas a los programas. Tiene
unas variables que permiten personalizar las ejecuciones para facilitar los benchmark.
Despues realiza 4 bucles 1 para cada tipo de ejecucion en donde se ejecuta 11 veces seguidas
cada programa con cada configuracion para de esta forma utilizar la primera ejecucion a modo
de calentamiento(luego sera descartada a la hora de hacer los graficos).


#!/usr/bin/env bash

#Script que automatiza el benchmark, probando diferente
#número de threads, tamaños de array y operaciones disponibles.

#Estos parametros se pueden cambiar a elección propia
threads_maximos=4
ejec=900000000
incremento=1
inicio=1
ejecucion=p1
ejecuciontotal=p1Total
op=sum

#n procesos

printf "numero de procesos antes de ejecutar\n"
ps aux --no-heading | wc -l
i=10

#secuencial con tiempo parcial

	for i in `seq $inicio $incremento 11`;do
		printf "ejecucion secuencial $i\n"
				./$ejecucion $ejec $op
	done

#paralelo con tiempo parcial

	for i in 1 2 3 4 8 10; do
		printf "\nnumero de threads= $i\n\n"
			for j in `seq $inicio $incremento 11`; do
					./$ejecucion $ejec $op --multi-thread $i
		done
	done

#java

	printf "ejecucion con conector de java\n"
	for i in 1 2 3 4 8 10; do
		printf "\nnumero de threads= $i\n"
			for j in `seq $inicio $incremento 11`; do
				time (cd /home/mario/Music/p1Conector/ && make run N_THREADS=$i N_ELEMENTOS=$ejec)
				done
		done

#paralelo con tiempo total

		printf "ejecucion paralela total\n"
		for i in 1 2 3 4 8 10; do
			printf "\n numero de threads= $i\n"
				for j in `seq $inicio $incremento 11`; do
					 time ./$ejecuciontotal $ejec $op --multi-thread $i
			done
		done

#nprocesos

  printf "numero de procesos despues de ejecutar\n"
  ps aux --no-heading | wc -l

El primer bucle hace la ejecucion del programa en modo secuencial.

El segundo bucle hace la ejecucion del programa en modo paralelo con 1, 2, 3, 4, 8 y 10 hilos
para de esta forma poder ver las diferencias entre pocos y muchos hilos hasta ser demasiados
para mi cpu.

El tercer bucle hace la ejecucion del codigo con el conector de java con 1, 2, 3, 4, 8 y 10
hilos, utilizando la funcion time de la bash para calcular el tiempo total de la ejecucion y
poder comparar las diferencias con el codigo sin conector de java.

El cuarto bucle es igual que el tercero solo que utilizando la funcion time de la bash para
poder compararlo con el conector de java de forma justa.

Al principio y al final del programa se cuentan los procesos abiertos para de esta forma
ver si las ejecuciones estan en situaciones similares.

BENCHMARK

Las ejecuciones que decidi realizar es la de 900000000 elementos con y sin el modo
FEATURE_OPTIMIZE. He elegido este numero ya que en la ejecucion secuencial tardaba
aproximadamente 4.5 segundos que me parece un numero lo suficientemente grande como para
ver claramente las diferencias de paralelizacion. Elegi la operacion suma para realizar los
benchmarks al igual que podria haber elegido cualquiera de las otras dos.

Para las ejecuciones para comparar el codigo paralelo con el secuencial he hecho una medicion
del tiempo en las Region Of Interest (ROI) quitando de esta forma los tiempos que tienen en
comun ambas ejecuciones.Sin embargo en las ejecuciones para comparar el conector de java
con las ejecuciones paralelas la medicion del tiempo se hace utilizando el tiempo "real"
de la ejecucion que proporciona la funcion time de la bash para de esta forma poder medir el
overhead que introduce el conector con java.

ANALISIS

Para realizar el analisis de los tiempos obtenidos he decidido utilizar excel ya que es facil
de usar y se puede hacer todo tipo de graficas sin ningun inconveniente. No he comparado la
ejecucion con FEATURE_OPTIMIZE con las demas ya que despues de ver los resultados al compararla
con la paralela llegue a la conclusion de que no merecia la pena porque no aportaba nada nuevo.
Lo primero, he pasado los datos impresos en consola a un fichero de texto desde el que
lo he pasado al excel donde he generado las siguientes graficas:

GRAFICA SPEEDUP SECUENCIAL/PARALELA

![](/p1/imagenes/
speedup%20secuencial.png)

Esta grafica representa el speedup de la media de los tiempos de ejecucion del programa respecto
del tiempo de ejecucion del programa secuencial. Lo primero que podemos observar es que como
es logico el speedup del tiempo secuencial es 1 y tambien que el de 1 thread es casi 1. esto
se debe a que al hacerlo con 1 thread se tiene que levantar ese hilo, lo que penaliza el tiempo
para luego no dar ninguna ventaja. A partir de ahi podemos ver como con 2, 3 y 4 hilos, la
ejecucion se hace casi 2, 3 y 4 veces mas rapido, esto se debe a que el procesador cuenta con 4
cores fisicos que pueden aprovechar todos sus recursos de forma completa. Al subira 8 hilos ya
empezamos a utilizar cores logicos lo que hace que el speedup no llegue a 8 ya que estos cores
tienen que compartir recursos entre ellos y los cores fisicos, aun asi nos proporciona un speedup
de casi 6. Sin embargo al subir a 10 hilos ya nos vemos penalizados por el hecho de que son mas hilos
que los cores de los que disponemos, esto hace que la gestion de los hilos raelentize la ejecucion.

GRAFICA EFICIENCIA SECUENCIAL/PARALELA

![](/p1/imagenes
/eficiencia%20secuencial.png)

En esta grafica podemos interpretar lo mismo que he explicado en la anterior grafica. Nos aporta con
mas claridad el hecho de que la eficiencia decae cuando se empiezan a utilizar cores logicos y se
empieza a desplomar cuando nos pasamos del total de cores.

GRAFICA SPEEDUP JAVA/PARALELA

![](/p1/imagenes
/speedup%20java.png)

En esta grafica comparamos las ejecuciones paralelas con ellas mismas ejecutadas a traves de java.
antes de mirar la grafica ya podemos imaginarnos que esto introducira un overhead y hara que sea mas
lento. Cuando miramos la grafica, como habiamos supuesto, la ejecucion sin el conector de java supone
un speedup respecto a utilizar el conector con java. Este speedup es muy pequeño, esto se debe a que es
un numero alto de iteraciones, si probasemos con un numero menor de iteraciones(no lo he hecho por
falta de tiempo), veriamos que el conector con java introduce un gran overhead.

GRAFICA SPEEDUP CON FEATURE_OPTIMIZE/ SIN FEATURE_OPTIMIZE

![](/p1/imagenes
/speedup%20optimize.png)

En esta grafica podemos ver, como adelante en la introduccion, que la diferencia de ejecutar con
FEATURE_OPTIMIZE a ejecutar sin ello es minima. Esto se debe a que la introduccion de un atomico
no aporta una gran ventaja ya que se hacen muy pocos accesos a dicha variable(en realidad se hace
1 acceso por cada hilo). Si se hiciese un gran numero de accesos veriamos una mejora bastante
grande al ejecutar con FEATURE_OPTIMIZE.

## 4. Discusion
En este apartado se aportara una valoracion personal de la practica, en la que se destacaron
aspectos como las dificultades encontradas en su realizacion y como se han resuelto.
tambien se respondera a las cuestiones formuladas si las hubiere.

Han habido innumerables fallos y contratiempos. La mayor parte de ellos han sido en el FEATURE_LOGGER
y a la hora de realizar benchmarks. Para resolverlos he hecho multitud de pruebas, he buscado en la red,
he preguntado a compañeros de clase y cuando no encontraba nada he preguntado al profesor.

## 5. Propuestas optativas
