#!/usr/bin/env bash

#Script que invoca al benchmark para comprobar diferentes ejecuciones, redirigiendo la salida a un fichero.
#Información: únicamente se redirige la segunda iteración (la primera calienta la CPU)

#Variables usadas para redirigir los ficheros y comparar el caso de ejecución
carpeta=build

if [ -d $carpeta ]; #se comprueba que existe el directorio contenedor del ejecutable
then
	make clean
	printf "Build directory has been deleted.\n"
fi

#Se eliminan los ficheros de resultados que devuelve el ejecutable
rm *.dat

make build #compilacion del ejecutable (accede a la carpeta src)

for iterations in 1 2; do
		printf "estacion $iterations\n"
		./benchsuite.sh
		sleep 1
	done
#printf "Iteration $iterations of all the cases finished.\n"
#sleep 1
