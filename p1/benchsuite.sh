#!/usr/bin/env bash

#Script que automatiza el benchmark, probando diferente
#número de threads, tamaños de array y operaciones disponibles.

#Estos parametros se pueden cambiar a elección propia
threads_maximos=4
#2 sec secuencial
#ejecucion1
ejec=900000000
#ejecucion2
#ejec=200000000
#ejecucion3
#ejec=50000000
ejec=300


incremento=1
inicio=1
ejecucion=p1
ejecuciontotal=p1Total
op=sum
#n procesos
printf "numero de procesos antes de ejecutar\n"
ps aux --no-heading | wc -l
i=10
#secuencial con tiempo parcial
	for i in `seq $inicio $incremento 11`;do
		printf "ejecucion secuencial $i\n"
				./$ejecucion $ejec $op
	done
#paralelo con tiempo parcial
	for i in 1 2 3 4 8 10; do
		printf "\nnumero de threads= $i\n\n"
			for j in `seq $inicio $incremento 11`; do
					./$ejecucion $ejec $op --multi-thread $i
		done
	done
# java
	printf "ejecucion con conector de java\n"
	for i in 1 2 3 4 8 10; do
		printf "\nnumero de threads= $i\n"
			for j in `seq $inicio $incremento 11`; do
				time (cd /home/mario/Music/p1Conector/ && make run N_THREADS=$i N_ELEMENTOS=$ejec)
				done
		done
paralelo con tiempo total
		printf "ejecucion paralela total\n"
		for i in 1 2 3 4 8 10; do
			printf "\n numero de threads= $i\n"
				for j in `seq $inicio $incremento 11`; do
					 time ./$ejecuciontotal $ejec $op --multi-thread $i
			done
		done
#nprocesos
printf "numero de procesos despues de ejecutar\n"
ps aux --no-heading | wc -l
