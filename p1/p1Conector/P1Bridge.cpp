#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <math.h>
#include <mutex>
#include <sys/time.h>
#include <time.h>
#include <condition_variable>
#include <stdbool.h>
#include <atomic>
#include <iostream>
#include "P1Bridge.h"


#define FEATURE_LOGGER 0
#define DEBUG 0
#define FEATURE_OPTIMIZE 1

struct Parametros{
  int nHilo;
  int inicio;
  int limite;
  int inicioExtra;
  int limiteExtra;
  double *array;
  double *resultadoLogger;
  double *resultado;
};

double resultadoAtomic= 0;
std::atomic<double> resultadoAtomico(resultadoAtomic);//variable atomica donde se guardara el resultado

std::mutex r_m;//result mutex
std::mutex x_m;//extra mutex

std::condition_variable hilo_acaba;//variable condicional que notifica que ha acabado un hilo
std::condition_variable logger_ready;//variable condicional que se notifica cuando el logger esta listo para recibir

bool loggerAcaba = false;//que se cambia cuando ha acabado el logger
bool g_ready = false;//se cambia cuando un hilo ha acabado
bool l_ready = false;//se cambia cuando el logger esta listo

int nHilo=0;//numero de hilo

void featureLogger(double* resTotal, int nHilosOp, std::condition_variable *logger_acaba, int op);
void sum (struct Parametros parametros);
void sub (struct Parametros parametros);
void xorr (struct Parametros parametros);

/**
* funcion main
*/
int todo (int longitud, char* ope, char* multic , int threadsP){
  int arrayLength = longitud-1;//atoi convierte el string a numero
  //creacion del array en memoria dinamica
  double* array = (double*)malloc(arrayLength * sizeof(double));
  //inicializacion del array
  for(int i=0 ; i<=arrayLength ; i++){
    array[i] = i;
    #if DEBUG == 1
    printf("array[%d]=%f\n",i,array[i]);
    #endif
  }
  char* operacion = ope;
  //creacion del struct para hacer benchmark
  double t;
  if(!strcmp(multic,"--multi-thread")){
    int nHilos = threadsP;
    int nHilosOp = nHilos;//nHilosOp contiene los hilos que van a operar(todos menos 1 en caso de que este activado el logger)

    #if FEATURE_LOGGER == 1
    nHilosOp = nHilos-1;
    #endif
    //el numero de hilos maximos es 12 y el minimo 1
    if(nHilos>12 || nHilos<1){
      printf("el numero de hilos  no esta entre 1 y 12\n");
      return 0;
    }
    //si el numero de hilos es mayor
    if(nHilosOp>arrayLength){
      nHilosOp = arrayLength;
      printf("el numero de hilos es igual a el numero de operaciones\n");
    }
    if(FEATURE_LOGGER == 1 && nHilos== 1){
      nHilos++;
      nHilosOp++;
    }
    #if DEBUG == 1
    printf("va a utilizar %d hilos\n", nHilos);
    #endif

    int resto = arrayLength%nHilosOp;//numero de operaciones que realizara el hilo que antes termine como operaciones extra
    #if DEBUG == 1
    printf("resto: %d\n",resto);
    #endif

    int tamHilo= floor(arrayLength/nHilosOp);//numero de operaciones que realizara cada hilo
    //creacion de los structs
    struct Parametros parametros[nHilos];
    int inicioExtra;//puntero en el array de numeros al donde comienza la parte extra
    int limiteExtra;//puntero en el array de numeros a donde finaliza la parte extra
    //si no es exacta inicializa las variables
    if(resto != 0){
      inicioExtra = arrayLength-resto+1;
      limiteExtra = arrayLength;
    } else{
      inicioExtra = -1;
      limiteExtra = -1;
    }

    #if FEATURE_LOGGER == 1
    std::condition_variable logger_acaba;//indica al main cuando acaba el logger
    double resultadoLogger = 0;
    int op;// indica la operacion que tendra que hacer el logger sum(0) sub(1) xor(2)
    #endif
    //comienzo del benchmark
    double resultado =0;
    //inicializacion de los structs
    std::thread threads[nHilos];
    for(auto i=0;i<nHilos;i++){
      parametros[i].inicio = i*tamHilo+1;
      parametros[i].limite = i*tamHilo+tamHilo;
      parametros[i].inicioExtra = inicioExtra;
      parametros[i].limiteExtra = limiteExtra;
      parametros[i].array = array;
      parametros[i].resultado = &resultado;
      parametros[i].nHilo = i;
      #if FEATURE_LOGGER == 1
        parametros[i].resultadoLogger = &resultadoLogger;
      #endif
      //elige la operacion e inicializa los hilos
      if(!strcmp(operacion,"sum")){
        #if FEATURE_LOGGER == 1
          if(i == nHilos-1){
            op = 0;
            threads[nHilos-1] = std::thread(featureLogger,&resultadoLogger,nHilosOp, &logger_acaba,op);
          }else{
            threads[i] = std::thread(sum,parametros[i]);
          }
        #else
          threads[i] = std::thread(sum,parametros[i]);
        #endif
      }
      if(!strcmp(operacion,"sub")){
        #if FEATURE_LOGGER == 1
          if(i == nHilos-1){
            op = 1;
            threads[nHilos-1] = std::thread(featureLogger,&resultadoLogger,nHilosOp, &logger_acaba,op);
          }else{
            threads[i] = std::thread(sub,parametros[i]);
          }
        #else
          threads[i] = std::thread(sub,parametros[i]);
        #endif
      }
      if(!strcmp(operacion,"xor")){
        #if FEATURE_LOGGER == 1
          if(i == nHilos-1){
            op = 2;
            threads[nHilos-1] = std::thread(featureLogger,&resultadoLogger,nHilosOp, &logger_acaba,op);
          }else{
            threads[i] = std::thread(xorr,parametros[i]);
          }
        #else
          threads[i] = std::thread(xorr,parametros[i]);
        #endif
      }
    }
    //join sin feature logger
    #if FEATURE_LOGGER == 0
      for(auto i=0; i<nHilos;i++){
        threads[i].join();
      }
      //finaliza el benchmark
      #if DEBUG == 1
        printf("el resultado es: %lf \n", resultado);
      #endif
      return 0;
    #endif
    //finalizacion del logger
    #if FEATURE_LOGGER == 1
      for(int i=0; i<nHilosOp;i++){
        threads[i].join();
      }
      //#if DEBUG == 1
      //printf("el resultado atomico es: %u \n", unsigned(resultadoAtomico));
      printf("el resultado de los hilos es: %f\n",resultado);
      printf("el resultado del logger es: %f\n", resultadoLogger);
      //#endif
      #if FEATURE_OPTIMIZE == 1
        //el atomico al estar casteado a unsigned no tiene precision suficiente para representar
        // numeros grandes de 92681 iteraciones o 32 bits
        #if DEBUG == 1
          printf("el resultado atomico es: %u \n", unsigned(resultadoAtomico));
          printf("el resultado de los hilos es: %f\n",resultado);
          printf("el resultado del logger es: %f\n", resultadoLogger);
        #endif
      #endif
      #if FEATURE_OPTIMIZE == 0
        #if DEBUG == 1
          printf("el resultado por los hilos es: %lf \n", resultado);
        #endif
      #endif
      {
        std::unique_lock<std::mutex> mainLoggerLock(r_m);
        logger_acaba.wait(mainLoggerLock,[] {return loggerAcaba;});//esperamos a que el logger acabe
        mainLoggerLock.unlock();
        threads[nHilos-1].join();//hacemos join al logger
        #if DEBUG == 1
          printf("join al logger\n");
        #endif
        #if FEATURE_OPTIMIZE == 0
          if(resultadoLogger != resultado){
            printf("los resultados del logger y los hilos no coinciden\n");
            return 1;
          }
        #endif
        #if FEATURE_OPTIMIZE == 1
          if(resultadoLogger != resultadoAtomico){
            printf("los resultados del logger y el resultado atomico no coinciden\n");
            return 1;
          }
        #endif

        printf("los resultados del logger y el resultado del atomico coinciden\n");
        return 0;
      }
    #endif

  }else{//ejecucion secuencial
    double total;
    if(strcmp(operacion,"sum") == 0){
      for(int i=0; i<arrayLength;i++){
        total = total+array[i];
      }
    }
    if(strcmp(operacion,"sub") == 0){
      for(int i=0; i<arrayLength;i++){
        total= total-array[i];
      }
    }
    if(strcmp(operacion,"xor") == 0){
      for(int i=0; i<arrayLength;i++){
        total = total+array[i];
      }
    }
  }
  return 0;
}
#if FEATURE_LOGGER == 1
void featureLogger(double* resTotal, int nHilosOp, std::condition_variable *logger_acaba, int op){

  int cont = nHilosOp;//cuenta los hilos que quedan por sumar
  double arrayLogger[nHilosOp];//array en el que se guardan los resultados que le pasan los hilos
  std::unique_lock<std::mutex> loggerLock(r_m);
  while(cont>0){
    l_ready = true;
    //notifica a los hilos que esta listo para recibir
    logger_ready.notify_all();
    hilo_acaba.wait(loggerLock,[] {return g_ready;});//espera a que le avise algun hilo
    arrayLogger[nHilo]=*resTotal;//guarda el resultado
    cont--;
    g_ready = false;
  }
  *resTotal = 0;
  for(int i =0; i<nHilosOp; i++){
    if(op == 0){
      *resTotal += arrayLogger[i];//suma el total del resultado
    }
    if(op == 1){
      *resTotal -= arrayLogger[i];//suma el total del resultado
    }
    if(op == 2){
      *resTotal =(int)*resTotal ^ (int)arrayLogger[i];//suma el total del resultado
    }
  }
  loggerAcaba = true;
  logger_acaba->notify_one();
}
#endif


void sum (struct Parametros parametros){
  double resultado = 0;
  //calcula el resultado de su region
  for(int i = parametros.inicio; i<=parametros.limite; i++){
    resultado = resultado + parametros.array[i];
  }
  //si es el primero en acabar toma el mutex y calcula la parte extra
  if(parametros.inicioExtra != -1 && x_m.try_lock()){
    for(int i = parametros.inicioExtra; i<=parametros.limiteExtra; i++){
      resultado = resultado + parametros.array[i];
    }
  }
  #if FEATURE_LOGGER == 1
    {
      std::unique_lock<std::mutex> sumLock(r_m);
      logger_ready.wait(sumLock,[] {return l_ready;});//espera a que el logger este listo
      l_ready = false;
      *parametros.resultadoLogger = resultado;
      g_ready=true;
      nHilo = parametros.nHilo;
      hilo_acaba.notify_one();//notifica al logger
    }
  #endif
  //printf("resultado %f\n", resultado);
  #if FEATURE_OPTIMIZE == 0
    std::lock_guard<std::mutex> guard(r_m);
    *parametros.resultado = *parametros.resultado + resultado;//guarda el resultado
  #endif
  #if FEATURE_OPTIMIZE == 1
    resultadoAtomico =resultadoAtomico+resultado;//guarda el resultado de forma atomica sin uso de mutex
  #endif
}

void sub (struct Parametros parametros){
  double resultado = 0;
  for(int i = parametros.inicio; i<=parametros.limite; i++){
    resultado = resultado + parametros.array[i];
  }
  #if DEBUG == 1
  printf("resultado despues del primer bucle de hilo %d = %f\n", parametros.nHilo,resultado);
  #endif
  if(parametros.inicioExtra != -1 && x_m.try_lock()){
    for(int i = parametros.inicioExtra; i<=parametros.limiteExtra; i++){
      resultado = resultado + parametros.array[i];
    }
  }

  #if DEBUG == 1
  printf("el resultado del hilo %d es %f \n",parametros.nHilo ,resultado);
  #endif
  resultado = resultado*-1;
  #if FEATURE_LOGGER == 1
    {
      std::unique_lock<std::mutex> sumLock(r_m);
      logger_ready.wait(sumLock,[] {return l_ready;});
      l_ready = false;
      *parametros.resultadoLogger = resultado;
      g_ready=true;
      nHilo = parametros.nHilo;
      hilo_acaba.notify_one();
      #if DEBUG == 1
      printf("el hilo %d ha notificado al logger\n",nHilo);
      #endif
    }
    #if DEBUG == 1
    printf("fin del hilo\n");
    #endif
  #endif
  #if FEATURE_OPTIMIZE == 0
    std::unique_lock<std::mutex> guard(r_m);
    *parametros.resultado = *parametros.resultado - resultado;
  #endif
  #if FEATURE_OPTIMIZE == 1
    resultadoAtomico =resultadoAtomico-resultado;
  #endif
}

void xorr (struct Parametros parametros){
  double resultado = 0;
  for(int i = parametros.inicio; i<=parametros.limite; i++){
    resultado = (int)resultado ^ (int)parametros.array[i];
  }
  #if DEBUG == 1
  printf("resultado despues del primer bucle de hilo %d = %f\n", parametros.nHilo,resultado);
  #endif
  if(parametros.inicioExtra != -1 && x_m.try_lock()){
    for(int i = parametros.inicioExtra; i<=parametros.limiteExtra; i++){
      resultado = (int)resultado ^ (int)parametros.array[i];
    }
  }

  #if DEBUG == 1
  printf("el resultado del hilo %d es %f \n",parametros.nHilo ,resultado);
  #endif
  #if FEATURE_LOGGER == 1
    {
      std::unique_lock<std::mutex> sumLock(r_m);
      logger_ready.wait(sumLock,[] {return l_ready;});
      l_ready = false;
      *parametros.resultadoLogger = resultado;
      g_ready=true;
      nHilo = parametros.nHilo;
      hilo_acaba.notify_one();
      #if DEBUG == 1
      printf("el hilo %d ha notificado al logger\n",nHilo);
      #endif
    }
    #if DEBUG == 1
    printf("fin del hilo\n");
    #endif
  #endif
  #if FEATURE_OPTIMIZE == 0
    std::unique_lock<std::mutex> guard(r_m);
    *parametros.resultado = (int)*parametros.resultado ^ (int)resultado;
  #endif
  #if FEATURE_OPTIMIZE == 1
    resultadoAtomico =(int)resultadoAtomico^(int)resultado;
  #endif
}

JNIEXPORT int JNICALL Java_P1Bridge_compute(JNIEnv *env, jobject thisObj, int longitud, jstring ope, jstring multi, int threads) {
	const char *nativeString = env->GetStringUTFChars(ope, 0);
	char* operF = (char*)nativeString;
	nativeString = env->GetStringUTFChars(multi, 0);
	char* multiT = (char*)nativeString;

	todo(longitud, operF, multiT, threads);
}
