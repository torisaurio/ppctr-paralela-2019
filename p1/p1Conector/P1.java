public class P1 {
   public static void main(String[] args) {
     int longitud = Integer.parseInt(args[0]);
     String ope = args[1];
     String multi = args[2];
     int threads = Integer.parseInt(args[3]);
     P1Bridge p = new P1Bridge();
     int resultado = p.compute(longitud, ope, multi, threads);
     //System.out.println("Resultado: "+resultado);
   }
}
