/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class P1Bridge */

#ifndef _Included_P1Bridge
#define _Included_P1Bridge
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     P1Bridge
 * Method:    compute
 * Signature: (ILjava/lang/String;Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_P1Bridge_compute
  (JNIEnv *, jobject, jint, jstring, jstring, jint);

#ifdef __cplusplus
}
#endif
#endif
