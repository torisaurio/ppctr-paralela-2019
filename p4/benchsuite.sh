#!/usr/bin/env bash

#Script que automatiza el benchmark


#Estos parametros se pueden cambiar a elección propia
max=11
#n procesos
printf "numero de procesos antes de ejecutar\n"
ps aux --no-heading | wc -l

g++ -std=c++11 -D MAX=5 -fopenmp -o build/generator src/generator.c;
./build/generator;
printf "MAX=5\n"
#secuencial secuencial sin optimizar
printf "ejecucion codigo original\n"
	for i in `seq 1 1 $max`;
	do
		./build/origvideo_task
	done
printf "ejecucion codigo paralelo\n"
	for i in `seq 1 1 $max`;
	do
		./build/video_task
	done
g++ -std=c++11 -D MAX=10 -fopenmp -o build/generator src/generator.c;
./build/generator;
printf "MAX=10\n"
printf "ejecucion codigo original\n"
	for i in `seq 1 1 $max`;
	do
		./build/origvideo_task
	done
printf "ejecucion codigo paralelo\n"
#for i in `seq 1 1 $max`;
#do
		./build/video_task
	done

g++ -std=c++11 -D MAX=5 -fopenmp -o build/generator src/generator.c;
./build/generator;
printf "MAX=50\n"
#printf "ejecucion codigo original\n"
#	for i in `seq 1 1 $max`;
#	do
#		./build/origvideo_task
#	done
printf "ejecucion codigo paralelo\n"
	for i in `seq 1 1 $max`;
	do
		./build/video_task
	done

#n procesos
printf "numero de procesos antes de ejecutar\n"
ps aux --no-heading | wc -l
