#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include <sys/time.h>

void fgauss (int *, int *, long, long);

int main(int argc, char *argv[]) {

  struct timeval start, end;
     FILE *in;
     FILE *out;
     int i, j, size, seq = 8;
     int **pixels, **filtered;

     if (argc == 2) seq = atoi (argv[1]);

     in = fopen("build/movie.in", "rb");
     if (in == NULL) {
        perror("movie.in");
        exit(EXIT_FAILURE);
     }

     out = fopen("build/movie2.out", "wb");
     if (out == NULL) {
        perror("movie.out");
        exit(EXIT_FAILURE);
     }

     long width, height;

     fread(&width, sizeof(width), 1, in);
     fread(&height, sizeof(height), 1, in);

     fwrite(&width, sizeof(width), 1, out);
     fwrite(&height, sizeof(height), 1, out);

     pixels = (int **) malloc (seq * sizeof (int *));
     filtered = (int **) malloc (seq * sizeof (int *));

    for (i=0; i<seq; i++)
    {
        pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
        filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
    }
    gettimeofday(&start, NULL);

    omp_set_num_threads(8);
   i = 0;
   #pragma omp parallel default(none), private(size), shared(i,height,width,pixels,filtered,in,out,seq)
   {
     #pragma omp master
     {
       do
       {
          size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

          if (size)
          {
             #pragma omp task
             {
               fgauss (pixels[i], filtered[i], height, width);
             }

             if(i >= (seq-1)) {
               #pragma omp taskwait
                for(i = 0; i < seq; i++)
                fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
                 i = 0;
             } else {
               i++;
             }
          }
       } while (!feof(in));
    }
  }
  for(int x = 0; x < i; x++)
    fwrite(filtered[x], (height + 2) * (width + 2) * sizeof(int), 1, out);
    gettimeofday(&end, NULL);

    for (i=0; i<seq; i++)
    {
       free (pixels[i]);
       free (filtered[i]);
    }
    free(pixels);
    free(filtered);

    fclose(out);
    fclose(in);
    double t= (end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0;
    printf("Total time: %f ms\n", t);
    return EXIT_SUCCESS;
 }

 void fgauss (int *pixels, int *filtered, long height, long width)
 {
 	int y, x, dx, dy;
 	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
 	int sum;

 	for (y = 0; y < width; y++) {
 		for (x = 0; x < height; x++)
 		{
 			sum = 0;
 			for (dx = 0; dx < 5; dx++)
 				for (dy = 0; dy < 5; dy++)
 					if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < height))
 						sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
 			filtered[x*height+y] = (int) sum/273;
 		}
 	}
 }
