# Título de práctica

Autor Mario Abad Diaz
Fecha 18/12/1999

## Prefacio

Objetivos conseguidos

Breve descripción de lo que te ha parecido la práctica. Aprendizajes, dificultades, cosas interesantes, qué te hubiera gustado hacer o mejorarías, qué conceptos te gustaría haber practicado de C++/OMP, opinión sobre el informe, etc

La practica 4 me ha ayudado a aprende a trabajar con ficheros de los que no puedo ver el resultado. No ver el resultado hace que la depuracion sea realmente dificil ya que no puedes saber si que estas haciendo bien o mal, solamente cuentas con la herramienta diff que te dice si esta bien o esta mal pero en este caso como los ficheros son tan grandes ademas de ilegibles es imposible encontrar tu fallo en el codigo basandote en eso.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

Descripcion del sistema donde se realiza el *benchmarking*.

-Ejecucion sobre Ubuntu Desktop 16.04 LTS 64bits

-7,7GiB RAM(obtenido en opciones about this computer)

-4 Cores fisicos(obtenido en la web de intel)

-4 Cores logicos(obtenido en la web de intel)

-CPU Intel Core i7-7700HQ(obtenido en opciones about this computer)

-CPU 8 cores(obtenido en opciones about this computer)

-CPU @1.9GHz(fijada con cpufreq usermode)

-Cache size(obtenido en /proc/cpuinfo): 6144KB

-CPU flags(obtenidas en /proc/cpuinfo): fpu vme de pse tsc msr pae mce cx8 apic sep mtrr
 pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb
 rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid
  aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg
  fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave
  avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs
  ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2
  erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves
  dtherm ida arat pln pts hwp hwp_notify hwp_act_window
 hwp_epp md_clear flush_l1d

-numero de procesos promedio(obtenido con ps aux ?no-heading | wc -l)= 243


## 2. Diseño e Implementación del Software

En este apartado se describirá la estructura del software desarrollado justificando las  decisiones que se hayan tomado en su diseño.

Si se cree necesario, se puede aportar código fuente, pero lo normal será describir los objetos que se propongan para desarrollar la práctica y la funcionalidad de los mismos, ya que el código fuente se entregará por separado.

Si se quiere enseñar código fuente se puede aportar en bloques de código simplificado (solo lo relevante, tal y como yo hago en las diapositivas del Tema 2) o indicar las líneas de código del fichero en cuestión. Ojo con la segunda opción ya que cada vez que toques el código fuente podrán cambiar.

En esta practica no ha habido que desarrollar codigo como en la 1 y la 2, sin embargo hemos tenido que paralelizar un codigo dado utilizando directivas de openmp. Concretamente utilizando parelelizacion asincrona. Para ello he utilizado las tasks que nos proporciona openmp.
Las tasks funcionan dentro de una region paralela #pragma omp parallel. Esta region paralela lo que hace es crear el numero de hilos que se le indique con la variable de entorno OMP_NUM_THREADS o mediante la funcion de runtime omp_set_num_threads() o si no se le define ninguna de las anteriores coge por defecto el numero de hilos que te retornaria omp_get_max_threads() que generalmente es el numero de cores de tu sistema. Al utilizar default(none) te obliga a etiquetar todas las variables que hay dentro de la region paralela. En este caso no hay necesidad de variables privadas ya que la region paralela solamente la ejecuta el master que va creando tareas. En las tareas las variables se pueden compartir ya que se utilizan arrays en los que cada thread utiliza posiciones distintas por lo que no se pisan unos a otros.
Dentro de la region paralela encontramos la directiva pragma omp master. Esta directiva solo permite que entre en la region el hilo maestro y hace que al salir de ella todos los hilos se sincronicen.
Despues encontramos la directiva pragma omp task que es la encargada de crear las tareas. Basicamente lo que se hace en el codigo es que el hilo main cree tareas y las lance para que los demas hilos las vayan ejecutando y como el programa trata de filtrar las imagenes de un video, las imagenes deben de estar ordenadas por lo que se utiliza un buffer en el que se guardan las imagenes filtradas para que cuando este el buffer lleno el hilo main utilizando la directiva pragma omp taskwait(que hace que el hilo que la llama espere hasta que finalicen todas las tareas) espera a que se terminen las tareas y asi escribirlas en el fichero de salida y vaciar el buffer.
Para que esto funcione el hilo main lanza un numero de tareas igual al tamaño del buffer para que no se pierdan imangenes.
Cuando se termina de leer el fichero de entrada, se termina la region paralela pero todavia pueden quedar imagenes sin escribir en el buffer para lo que se incluye un bucle for que termina de vaciarlo.
Se utiliza un buffer debido a que el tamaño de las imagenes puede ser muy grande y en un video hay muchas imagenes por lo que se podria saturar la memoria del sistema.

## 3. Metodología y desarrollo de las pruebas realizadas

Descripción detallada del proceso de benchmarking, captura de resultados y generación de gráficas. Todo lo necesario para comenzar el análisis.

Aquí se describirán las pruebas realizadas, su intención y los resultados obtenidos. Se incluirá el desarrollo de programas de prueba adicionales si han sido necesarios.

Podéis incluir vuestros scripts de ejecución en el propio informe (o llevarlos a Anexos si lo crees conveniente), así como indicar cómo los habéis utilizado y por qué:

Los metodos de benchmarking son los mismos a los utilizados en anteriores practicas.

Comienzo la practica utilizando 8 hilos ya que en principio es lo optimo para mi ordenador. Hago pruebas cambiando la variable max, que indica el numero de imagenes, por 5, 10 y 50.

GRAFICA SPEEDUP 8 HILOS PARALELO SOBRE SECUENCIAL

![](./imagenes/speedup8hilos4hilos.png)

A la vista de la grafica podemos ver como al aumentar el numero de imagenes el rendimiento del programa paralelo mejora, sin embargo, este speedup no supera el valor de 4, esto se debe a que la cpu cuenta con 4 cores fisicos y los otros 4 son logicos. Al compartir hardware, los cores logicos no pueden aprovechar su potencial ya que no disponen de las herramientas para procesar datos todo el tiempo. Para confirmar esto lleve a cabo un nuevo benchmark, esta vez con 4 hilos en la version paralela.

GRAFICA SPEEDUP 8 HILOS SOBRE 4 HILOS

![](./imagenes/speedup8hilossecuencial.png)

Confirmo con esta grafica el hecho de que los 8 cores no son aprovechados. Muy probablemente sea por lo mencionado en la anterior grafica.
## 4. Discusión

En este apartado se aportará una valoración personal de la práctica, en la que se destacarán aspectos como las dificultades encontradas en su realización y cómo se han resuelto.

También se responderá a las cuestiones formuladas si las hubiere.

En esta practica la mayor dificultad ha sido no saber donde estaban los errores en el codigo ya que el unico mecanismo para comprobar que el codigo esta bien es realizar multiples ejecuciones y comparar con el comando diff los ficheros resultantes. Para resolver este problema he realizado un pequeño programa para simplificar el problema y poder ver los errores.

6 ¿Se te ocurre alguna optimizacion sobre la funcion fgauss? Explica que has hecho
y que ganancia supone con respecto a la version paralela anterior.

Para mejorar la funcion fgauss he cambiado el indice de las x por el de las y. Esto permite un mejor aprovechamiento de la memoria cache, lo que genera un speedup de 1.1 aproximadamente.
