# Título de práctica

Autor Mario Abad Diaz
Fecha 20/12/2019

## Prefacio

Objetivos conseguidos

Breve descripción de lo que te ha parecido la práctica. Aprendizajes, dificultades, cosas interesantes, qué te hubiera gustado hacer o mejorarías, qué conceptos te gustaría haber practicado de C++/OMP, opinión sobre el informe, etc

La practica 5 ha sido una practica en la que he aprendido a hacer profiling utilizando las herramientas colgadas en moodle. Tambien he visto lo util que es openmp a la hora de paralelizar un programa sin tener que pasar por los esfuerzos que requiere hacerlo gestionando a mano los hilos.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

Descripcion del sistema donde se realiza el *benchmarking*.

-Ejecucion sobre Ubuntu Desktop 16.04 LTS 64bits

-7,7GiB RAM(obtenido en opciones about this computer)

-4 Cores fisicos(obtenido en la web de intel)

-4 Cores logicos(obtenido en la web de intel)

-CPU Intel Core i7-7700HQ(obtenido en opciones about this computer)

-CPU 8 cores(obtenido en opciones about this computer)

-CPU @1.9GHz(fijada con cpufreq usermode)

-Cache size(obtenido en /proc/cpuinfo): 6144KB

-CPU flags(obtenidas en /proc/cpuinfo): fpu vme de pse tsc msr pae mce cx8 apic sep mtrr
 pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb
 rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid
  aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg
  fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave
  avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs
  ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2
  erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves
  dtherm ida arat pln pts hwp hwp_notify hwp_act_window
 hwp_epp md_clear flush_l1d

-numero de procesos promedio(obtenido con ps aux ?no-heading | wc -l)= 242


## 2. Diseño e Implementación del Software

En este apartado se describirá la estructura del software desarrollado justificando las  decisiones que se hayan tomado en su diseño.

Si se cree necesario, se puede aportar código fuente, pero lo normal será describir los objetos que se propongan para desarrollar la práctica y la funcionalidad de los mismos, ya que el código fuente se entregará por separado.

Si se quiere enseñar código fuente se puede aportar en bloques de código simplificado (solo lo relevante, tal y como yo hago en las diapositivas del Tema 2) o indicar las líneas de código del fichero en cuestión. Ojo con la segunda opción ya que cada vez que toques el código fuente podrán cambiar.

MAIN

Comenzamos creando una region paralela utilizando la directiva pragma omp parallel en la que asignamos 8 como el numero de hilos(en benchmaking veremos porque) y una serie de variables como private y otras como shared. dentro de las privadas tenemos x e y que son parametros de explode que son propios a cada iteracion del bucle en el que se llama, i y j que son variables que controlan las iteraciones de varios bucles for y c que es una variable que se cambia dentro de otro for. Las variables que son shared no producen conflictos en el funcionamiento del programa por estar compartidas entre los hilos.

Para paralelizar el bucle donde se llama a explode utilizo dentro de la region paralela del main la instruccion pragma omp for que lo que hace es que el bucle for que tenga inmediatamente a continuacions se paralelize repartiendo las iteraciones entre los hilos. Para poder controlar la forma de repartir el trabajo temos la clausula schedule que tiene 3 posibles modos. El primero es static que lo que hace es dividir el trabajo en el numero de fragmentos que se le pase como parametro. El segundo es dynamic que divide las iteraciones en fragmentos del tamaño que se le especifique como parametro. Por ultimo tenemos guided que crea framentos con un tamaño decreciente segun la formula N/(k*t) siendo N el numero de iteraciones restante en cada momento, k un valor que podemos pasar como parametro y T en numero de hilos. En este caso utilizo dynamic ya que es la que mejor rendimiento me ha dado al realizar pruebas. el tamaño de los bloques lo establezco a un numero pequeño porque es el que mejor rendimiento ha dado en las pruebas. No hay una gran diferencia de tiempos entre utilizar bloques de tamaño 1 y  bloques de tamaño 10.

La siguiente seccion la debe ejectar el hilo que primero llegue ya que se inicializa una variable a 0 que sera posteriormente utilizada y generaria problemas si los hilos la volviesen a poner a 0.

Despues encontramos otra region que se puede procesar de 4 formas distintas como se indica en el ejercicio 3.

La primera forma que encontramos es la de openm en la que se paraleliza la region utilizando un for y dentro de el una region critica que se crea con la directica pragma omp critical que lo que hace es garantizar la exclusion mutua del codigo que contenga.

La segunda forma se hace utilizando funciones del runtime. Concretamente utilizo las funciones para administrar un lock(un mutex del runtime). Declaro un lock y lo inicializo con la funcion omp_init_lock. A continuacion creo un bucle for con la directiva pragma omp for y dentro de el for utilizo las funciones omp_set_lock y omp_unset_lock para bloquear y desbloquear el mutex garantizando la exclusion mutua en la gestion de la variable c_max.

La tercera forma es crear el bucle dentro de una seccion pragma omp single que lo que hace es dejar que ejecute la seccion el primer hilo que llegue a ejecutarla de forma que el bucle se ejecuta de forma secuencial.

La cuarta forma es utilizando variables privadas para ello se crea un for con pragma omp for y asignando como privadas las variables i y j ademas de haciendo recduction con operacion max de la variable c_max.

Se declaran las variables r g b dentro de una region single para que se declaren una sola vez y no se pisen.

por ultimo se utiliza un pragma omp for para paralelizar el ultimo bucle for del main.

## 3. Metodología y desarrollo de las pruebas realizadas

Descripción detallada del proceso de benchmarking, captura de resultados y generación de gráficas. Todo lo necesario para comenzar el análisis.

Aquí se describirán las pruebas realizadas, su intención y los resultados obtenidos. Se incluirá el desarrollo de programas de prueba adicionales si han sido necesarios.

Podéis incluir vuestros scripts de ejecución en el propio informe (o llevarlos a Anexos si lo crees conveniente), así como indicar cómo los habéis utilizado y por qué:

Utilizando los resultados de la practica 1 que dio 8 como numero de hilos optimo para mi ordenador, realizare los benchmarks (c++ y omp) de forma que utilizen 8 hilos ya que el objetivo sobre todo es comparar cpp con omp y la version secuencial dada con la optimizada, comparar hilos con la secuencial es algo mas secundario ya que ya se hizo en la practica 1.

Para realizar los benchamarks se han utilizado 2 scripts de la misma manera que en la practica anterior. tenemos el script automatized_exec_opt.sh que es exactamente igual al de la practica anterior ya que solo llama al programa 2 veces esperando 1 segundo entre llamadas. En el otro script se realizan llamadas al el programa con las distintas compilaciones, siendo el que utiliza la region que se cambia en el apartado 3 con variables privadas

SPEEDUP ORIGINAL VS PARALELO

![](./imagenes/origParalelo.png)

A la vista de esta grafica podemos concluir que la diferencia entre la paralelizacion a 8 hilos y a 4 hilos no es demasiado grande debido a la cantidad de trabajo que se ejecuta en secuencial. Tambien podemos ver como el incremento en el nuemro de iteraciones tambien supone un mejor aprovechamiento de las 8 cpu ya que esta mas tiempo ejecutando en paralelo. No he realizado benchmarks con mayor numero de iteraciones ya que tomarian demasiado tiempo.

SPEEDUP C_MAX

![](./imagenes/speedupc_max.png)

En esta segunda grafica podemos observar los diferentes rendimientos en la region de la actividad 3. Utilizo la ejecucion de variables privadas ya que es la que mejor rendimiento tiene.Comparo respecto de la ejecucion secuencial ya que es la mas estandar. En la primera y cuarta barra vemos como la ejecucion utilzando funciones del runtime da un resultado de rendimiento casi igual, lo que indica que el manejo del mutex del runtime es muy costoso haciendo que utilizar 8 hilos no mejore nada el rendimiento. En la segunda y quinta barra vemos un gran speedup que se corresponde a la ejecucion con variables privadas. Esto indica que el uso de variables privadas y la clausula reduction no penalizan mucho el tiempo. Por ultimo en la tercera y sexta barra vemos como el uso de la region critica da un rendimiento realmente malo haciendo que sea muy inferior al secuencial y dando un tiempo muy alto en comparacion al resto.


## 4. Discusión

En este apartado se aportará una valoración personal de la práctica, en la que se destacarán aspectos como las dificultades encontradas en su realización y cómo se han resuelto.

También se responderá a las cuestiones formuladas si las hubiere.

Para realizar el profiling he utilzado valgrind.
Lo primero que he hecho es compilar con -g.
He utilizado la instruccionvalgrind --tool=callgrind --dump-instr=yes para generar el fichero del profiling
Por ultimo con kcachegrind he abierto el fichero generado por valgrind y en el seleccionando el main y viendo la grafica he visto que la mayor parte del tiempo se estaba ejecutando el metodo explode.


En la actividad 1 se preguntan las lineas que no se ejeuctan. Para obtener estas lineas he utilizado la herramienta gcov. Para utilizarla he:
 g++ -std=c++11 -fprofile-arcs -ftest-coverage -g -o p5 p5.
ejecutamos para que se generen los ficheros auxiliares
./p5
obtenemos el analisis de cobertura
gcov p5.

dentro de el fichero .gcov identifico las lineas que no se ejecutan, estas lineas van precedidas por #####.
No se ejecutan las lineas en ppma_write dento de if(!file_out)
Dentro de ppma_write_header ninguno de los if(error).
timestamp se ejecuta en el codigo original pero en el mio lo tengo comentado por lo que el metodo no se llama nunca.
