#!/usr/bin/env bash
velGHz=1.90GHz
sudo cpufreq-set -f $velGHz -c 0
sudo cpufreq-set -f $velGHz -c 1
sudo cpufreq-set -f $velGHz -c 2
sudo cpufreq-set -f $velGHz -c 3
sudo cpufreq-set -f $velGHz -c 4
sudo cpufreq-set -f $velGHz -c 5
sudo cpufreq-set -f $velGHz -c 6
sudo cpufreq-set -f $velGHz -c 7

cpufreq-info
